package com.test.longestsubstr.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetofStrings {
	
	@NotNull(message = "Error in LongestsubstrRequest- setofStrings values should not be empty")
	private String value;

}
