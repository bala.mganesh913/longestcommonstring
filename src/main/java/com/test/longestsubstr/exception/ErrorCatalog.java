package com.test.longestsubstr.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCatalog
{
    APPLICATION_ERROR(500,"Application Error",ErrorLevel.TECHNICAL),
    RESOURCE_NOT_FOUND(404,"Resource not found",ErrorLevel.TECHNICAL),
    ACCESS_DENIED(401,"Access is Denied",ErrorLevel.FUNCTIONAL),
    BAD_DATA_ARGUMENT(400,"Bad Request",ErrorLevel.FUNCTIONAL),
    INVALID_INPUT(400,"Invalid Input",ErrorLevel.FUNCTIONAL);

    private int code;
    private String message;
    private ErrorLevel errorLevel;

}
