package com.test.longestsubstr.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.test.longestsubstr.dto.request.LongestsubstrRequestDTO;
import com.test.longestsubstr.dto.response.LongestsubstrResponseDTO;

public class LongetSubstrRequestDTOTest {

	public LongestsubstrRequestDTO should_check_LongetSubstrRequestDTO() {
		
		LongestsubstrRequestDTO longestsubstrRequestDTO = new LongestsubstrRequestDTO();
		List<SetofStrings> setofStringsLst = new ArrayList<SetofStrings>();
		SetofStrings setOfStr = new SetofStrings();
		setOfStr.setValue("comcast");
		setofStringsLst.add(setOfStr);
		setOfStr = new SetofStrings();
		setOfStr.setValue("comcastic");
		setofStringsLst.add(setOfStr);
		setOfStr = new SetofStrings();
		setOfStr.setValue("broadcaster");
		setofStringsLst.add(setOfStr);
		longestsubstrRequestDTO.setSetofStrings(setofStringsLst);
		
		return longestsubstrRequestDTO;
		
	}

	public LongestsubstrRequestDTO should_check_LongetSubstrRequestDTO1() {
		LongestsubstrRequestDTO longestsubstrRequestDTO = new LongestsubstrRequestDTO();
		List<SetofStrings> setofStringsLst = new ArrayList<SetofStrings>();
		SetofStrings setOfStr = new SetofStrings();
		setOfStr.setValue("comcast");
		setofStringsLst.add(setOfStr);
		setOfStr = new SetofStrings();
		setOfStr.setValue("communicate");
		setofStringsLst.add(setOfStr);
		setOfStr = new SetofStrings();
		setOfStr.setValue("commutation");
		setofStringsLst.add(setOfStr);
		longestsubstrRequestDTO.setSetofStrings(setofStringsLst);
		
		return longestsubstrRequestDTO;
	}

	public LongestsubstrRequestDTO should_check_LongetSubstrRequestDTO2() {
		LongestsubstrRequestDTO longestsubstrRequestDTO = new LongestsubstrRequestDTO();
	
		return longestsubstrRequestDTO;
	}

	public LongestsubstrRequestDTO should_check_LongetSubstrRequestDTO3() {
		return null;
	}

	public LongestsubstrRequestDTO should_check_LongetSubstrRequestDTO4() {
		LongestsubstrRequestDTO longestsubstrRequestDTO = new LongestsubstrRequestDTO();
		List<SetofStrings> setofStringsLst = new ArrayList<SetofStrings>();
		SetofStrings setOfStr = new SetofStrings();
		setOfStr.setValue("comcast");
		setofStringsLst.add(setOfStr);
		setOfStr = new SetofStrings();
		setOfStr.setValue("comcast");
		setofStringsLst.add(setOfStr);
		longestsubstrRequestDTO.setSetofStrings(setofStringsLst);
		
		return longestsubstrRequestDTO;
	}

}
