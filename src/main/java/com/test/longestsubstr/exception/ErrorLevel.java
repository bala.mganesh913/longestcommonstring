package com.test.longestsubstr.exception;

public enum ErrorLevel {
    FUNCTIONAL,
    TECHNICAL
}
