package com.test.longestsubstr.dto.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.test.longestsubstr.dto.SetofStrings;

import lombok.Data;

@Data
public class LongestsubstrRequestDTO {
	
    @Valid
    @NotNull(message = "Error in LongestsubstrRequest- setofStrings should not be empty")
    private List<SetofStrings>  setofStrings;

}
