package com.test.longestsubstr.common;

public class ApiUrl {
    public static final String API_ROOT_V1 = "/api/v1";
    public static final String COMMON_ROOT_LONGEST = API_ROOT_V1+"/lcs";
}
