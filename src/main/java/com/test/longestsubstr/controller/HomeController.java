package com.test.longestsubstr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

  @GetMapping(value = "/")
    public String home(){
      return "redirect:/swagger-ui.html";
  }

}
