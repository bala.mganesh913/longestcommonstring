package com.test.longestsubstr.dto.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.test.longestsubstr.dto.Lcs;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LongestsubstrResponseDTO {
	
	 private List<Lcs> lcs;

}
