package com.test.longestsubstr.controller;

import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.test.longestsubstr.common.ApiUrl;
import com.test.longestsubstr.dto.ErrorDto;
import com.test.longestsubstr.dto.SetofStrings;
import com.test.longestsubstr.dto.request.LongestsubstrRequestDTO;
import com.test.longestsubstr.dto.response.LongestsubstrResponseDTO;
import com.test.longestsubstr.exception.ErrorCatalog;
import com.test.longestsubstr.exception.InvalidInputException;
import com.test.longestsubstr.service.ILongestSubstrService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Api(value = "LongestSubstring service")
@RestController
@RequestMapping(value = ApiUrl.COMMON_ROOT_LONGEST,produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class LongestSubStrController {

	
@Autowired
ILongestSubstrService longestSubstrService;
   
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(tags = {"LongestSubstring service"},value = "LongestSubstring", notes = "LongestSubstring")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Successful operation"),
                    @ApiResponse(code = 400, message = "Bad Request", response = ErrorDto.class),
                    @ApiResponse(code = 401, message = "Unauthorized"),
                    @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorDto.class),
                    @ApiResponse(code = 503, message = "Service Unavailable", response = ErrorDto.class)
            }
    )
    public ResponseEntity<LongestsubstrResponseDTO> create(@Valid @NotNull @RequestBody LongestsubstrRequestDTO longestsubstrRequestDTO)
    {
        if(longestsubstrRequestDTO.getSetofStrings().size() == 0){
        	    log.error("setOfStrings Cannot be empty");
                throw new InvalidInputException(ErrorCatalog.INVALID_INPUT,"Enter Valid Data,setOfStrings should not be empty");
        }
        
        Map<SetofStrings, Long> value = longestsubstrRequestDTO.getSetofStrings().stream().collect(Collectors.groupingBy(k -> k, Collectors.counting()));
        for (Map.Entry<SetofStrings, Long> entry : value.entrySet()) {
            if(entry.getValue() > 1 ) {
            	log.error("setOfStrings Cannot be empty");
                throw new InvalidInputException(ErrorCatalog.INVALID_INPUT,"Enter Valid Data,all given strings are not unique");
            }
        }
        LongestsubstrResponseDTO response = longestSubstrService.retrieveLongestSubStr(longestsubstrRequestDTO);
            return ResponseEntity.ok(response);
    }
	
}
