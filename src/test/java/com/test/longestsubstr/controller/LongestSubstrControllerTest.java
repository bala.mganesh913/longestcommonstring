package com.test.longestsubstr.controller;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.longestsubstr.common.ApiUrl;
import com.test.longestsubstr.dto.LongetSubstrRequestDTOTest;
import com.test.longestsubstr.dto.request.LongestsubstrRequestDTO;
import com.test.longestsubstr.handler.ApiExceptionHandler;
import com.test.longestsubstr.handler.DefaultExceptionHandler;
import com.test.longestsubstr.service.ILongestSubstrService;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = LongestSubstrControllerTest.ApplicationTest.class)
@ActiveProfiles("test")
public class LongestSubstrControllerTest {

	
    @MockBean
    private LongestSubStrController longestSubStrController;

    @MockBean
    private ILongestSubstrService longestSubstrService;

    private LongetSubstrRequestDTOTest longetSubstrRequestDTOTest;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();


    @BeforeEach
    void setUp() {
        this.longetSubstrRequestDTOTest =  new LongetSubstrRequestDTOTest();
        mockMvc = standaloneSetup(longestSubStrController).setControllerAdvice(ApiExceptionHandler.class
                , DefaultExceptionHandler.class).build();

    }
    @Test
    @DisplayName("Post Longest SubString Test Case Scenario 1 - SUCCESS")
    @Rollback(value = false)
    public void post_longest_Sub_str_success() throws Exception {

    	LongestsubstrRequestDTO longestsubstrRequestDTO = longetSubstrRequestDTOTest.should_check_LongetSubstrRequestDTO();
        String body = objectMapper.writeValueAsString(longestsubstrRequestDTO);
            mockMvc.perform(MockMvcRequestBuilders.post(ApiUrl.COMMON_ROOT_LONGEST)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON).content(body)
                    .characterEncoding("UTF-8"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(result -> System.out.println(result));

    }
    
    @Test
    @DisplayName("Post Longest SubString Test Case Scenario 2 - SUCCESS")
    @Rollback(value = false)
    public void post_longest_Sub_str_success1() throws Exception {

    	LongestsubstrRequestDTO longestsubstrRequestDTO = longetSubstrRequestDTOTest.should_check_LongetSubstrRequestDTO1();
        String body = objectMapper.writeValueAsString(longestsubstrRequestDTO);
            mockMvc.perform(MockMvcRequestBuilders.post(ApiUrl.COMMON_ROOT_LONGEST)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON).content(body)
                    .characterEncoding("UTF-8"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(result -> System.out.println(result));

    }
    
    @Test
    @DisplayName("Post Longest SubString Test Case Empty SubString Scenario 3 - FAILURE")
    @Rollback(value = false)
    public void post_longest_Sub_str_Empty() throws Exception {

    	LongestsubstrRequestDTO longestsubstrRequestDTO = longetSubstrRequestDTOTest.should_check_LongetSubstrRequestDTO2();
        String body = objectMapper.writeValueAsString(longestsubstrRequestDTO);
            mockMvc.perform(MockMvcRequestBuilders.post(ApiUrl.COMMON_ROOT_LONGEST)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON).content(body)
                    .characterEncoding("UTF-8"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(result -> System.out.println(result));

    }
    
    @Test
    @DisplayName("Post Longest SubString Test Case No Body SubString Scenario 4 - FAILURE")
    @Rollback(value = false)
    public void post_longest_Sub_str_nobody() throws Exception {
    	
    	LongestsubstrRequestDTO longestsubstrRequestDTO = longetSubstrRequestDTOTest.should_check_LongetSubstrRequestDTO3();
        String body = objectMapper.writeValueAsString(longestsubstrRequestDTO);
            mockMvc.perform(MockMvcRequestBuilders.post(ApiUrl.COMMON_ROOT_LONGEST)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON).content(body)
                    .characterEncoding("UTF-8"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(result -> System.out.println(result));

    }
    
    @Test
    @DisplayName("Post Longest SubString Test Case Duplicate SubString Scenario 5 - FAILURE")
    @Rollback(value = false)
    public void post_longest_Sub_str_duplicate() throws Exception {
    	
    	LongestsubstrRequestDTO longestsubstrRequestDTO = longetSubstrRequestDTOTest.should_check_LongetSubstrRequestDTO4();
        String body = objectMapper.writeValueAsString(longestsubstrRequestDTO);
            mockMvc.perform(MockMvcRequestBuilders.post(ApiUrl.COMMON_ROOT_LONGEST)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON).content(body)
                    .characterEncoding("UTF-8"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(result -> System.out.println(result));

    }
    
    
	
	    @SpringBootApplication()
	    @ComponentScan({"com.test.*"})
	    public static class ApplicationTest
	    {

	    }
}
