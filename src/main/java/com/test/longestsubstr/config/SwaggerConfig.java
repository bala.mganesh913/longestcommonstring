package com.test.longestsubstr.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.test.longestsubstr.common.ApiUrl;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.Tag;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@ConditionalOnProperty(name = "swagger.enabled",havingValue = "true")
public class SwaggerConfig {
	
    @Value("${swagger.description}")
    private String description;

    @Value("${swagger.title}")
    private String swaggerTitle;

    @Value("${swagger.version}")
    private String applicationVersion;

    @SuppressWarnings("unused")
    private class AuthError{
        @JsonProperty("error")
        private String errorCode;
        @JsonProperty("error_description")
        private String description;


        public String getErrorCode()
        {
            return errorCode;
        }

        public String getDescription()
        {
            return description;
        }
    }

    @Autowired
    private Docket docket;
    @Autowired
    private TypeResolver typeResolver;

    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<String>(Arrays.asList("application/json"));

    private List<ResponseMessage> globalResponseMessages()
    {
        return Arrays.asList(
                new ResponseMessageBuilder().code(401).message("Unauthorized").build(),
                new ResponseMessageBuilder().code(404).message("Not Found").build(),
                new ResponseMessageBuilder().code(500).message("Unexpected condition encountered").build());
    }

    private ApiInfo getClientApiInfo()
    {
        return new ApiInfoBuilder().title(swaggerTitle).description(description).version(applicationVersion).build();
    }

    @PostConstruct
    void customizeUsersDocket()
    {
        docket.tags( new Tag("Longest Substring",""))
            .groupName("Longest Substring Service")
            .apiInfo(getClientApiInfo())
            .select()
            .paths(PathSelectors.regex(ApiUrl.API_ROOT_V1+".*"))
            .build().consumes(DEFAULT_PRODUCES_AND_CONSUMES).produces(DEFAULT_PRODUCES_AND_CONSUMES)
            .additionalModels(typeResolver.resolve(AuthError.class))
            .globalOperationParameters(getAuthHeadersParam())
                .globalResponseMessage(RequestMethod.GET,globalResponseMessages())
                .globalResponseMessage(RequestMethod.POST,globalResponseMessages())
                .globalResponseMessage(RequestMethod.PUT,globalResponseMessages())
                .globalResponseMessage(RequestMethod.DELETE,globalResponseMessages());

    }

    protected ArrayList<Parameter> getAuthHeadersParam()
    {
        ArrayList<Parameter> operationParameters = new ArrayList<>();
        operationParameters.add(new ParameterBuilder()
        .parameterType("header")
        .name("Authorization")
        .description("JWT Token")
        .modelRef(new ModelRef("string"))
        .required(false)
        .build());
        return operationParameters;
    }



}
