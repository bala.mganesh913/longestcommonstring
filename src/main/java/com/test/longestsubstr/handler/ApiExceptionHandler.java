package com.test.longestsubstr.handler;


import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.test.longestsubstr.dto.ErrorDto;
import com.test.longestsubstr.exception.ErrorCatalog;
import com.test.longestsubstr.exception.InvalidInputException;
import com.test.longestsubstr.exception.ServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiExceptionHandler {


    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<ErrorDto> handleServiceException(ServiceException exception){
        ErrorCatalog errorCatalog = exception.getErrorCatalog();
        int code = errorCatalog.getCode();
        String message = exception.getMessage();
        ErrorDto errorDetails = new ErrorDto(code,message);
        log.error("Service Exception !",exception);
        return ResponseEntity.status(getHttpStatus(errorCatalog)).body(errorDetails);
    }

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<ErrorDto> handleInvalidInputException(InvalidInputException exception){
        ErrorCatalog errorCatalog = exception.getErrorCatalog();
        int code = errorCatalog.getCode();
        String message = exception.getMessage();
        ErrorDto errorDetails = new ErrorDto(code,message);
        log.error("Invalid Input Exception !",exception);
        return ResponseEntity.status(getHttpStatus(errorCatalog)).body(errorDetails);
    }

    private HttpStatus getHttpStatus(ErrorCatalog errorCatalog)
    {
        switch (errorCatalog)
        {
            case BAD_DATA_ARGUMENT:
                return HttpStatus.BAD_REQUEST;
            case RESOURCE_NOT_FOUND:
                return HttpStatus.NOT_FOUND;
            default:
                return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
