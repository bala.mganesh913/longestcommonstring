package com.test.longestsubstr.service;

import javax.validation.Valid;

import com.test.longestsubstr.dto.request.LongestsubstrRequestDTO;
import com.test.longestsubstr.dto.response.LongestsubstrResponseDTO;

public interface ILongestSubstrService {

	LongestsubstrResponseDTO retrieveLongestSubStr(@Valid LongestsubstrRequestDTO longestsubstrRequestDTO);

}
