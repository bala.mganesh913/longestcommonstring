package com.test.longestsubstr.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.test.longestsubstr.dto.Lcs;
import com.test.longestsubstr.dto.request.LongestsubstrRequestDTO;
import com.test.longestsubstr.dto.response.LongestsubstrResponseDTO;
import com.test.longestsubstr.exception.ErrorCatalog;
import com.test.longestsubstr.exception.ServiceException;
import com.test.longestsubstr.service.ILongestSubstrService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ILongestSubstrServiceImpl implements ILongestSubstrService {

	@Override
	public LongestsubstrResponseDTO retrieveLongestSubStr(@Valid LongestsubstrRequestDTO longestsubstrRequestDTO) {
		LongestsubstrResponseDTO longestsubstrResponseDTO = new LongestsubstrResponseDTO();
		List<Lcs> lcs = new ArrayList<Lcs>();
		Lcs lcsValue = new Lcs();
		try {
			
			String strArr[] = longestsubstrRequestDTO.getSetofStrings().stream().map(e-> e.getValue()).toArray(String[]::new);;
			
			String commonStr="";
		    String smallStr ="";        

		    //identify smallest String      
		    for (String str :strArr) {
		        if(smallStr.length()< str.length()){
		            smallStr=str;
		        }
		    }

		    String tempCom="";
		    char [] smallStrChars=smallStr.toCharArray();               
		    for (char character: smallStrChars){
		        tempCom+= character;

		        for (String str :strArr){
		            if(!str.contains(tempCom)){
		                tempCom=character+"";
		                for (String str1 :strArr){
		                    if(!str1.contains(tempCom)){
		                        tempCom="";
		                        break;
		                    }
		                }
		                break;
		            }               
		        }

		        if(tempCom!="" && tempCom.length()>commonStr.length()){
		            commonStr=tempCom;  
		        }                       
		    }   
	           log.info("Longest common prefix: " + commonStr);
	           lcsValue.setValue(commonStr);
	           lcs.add(lcsValue);
	           longestsubstrResponseDTO.setLcs(lcs);
			
		} catch (Exception exception) {
			log.error("Error while retrieveLongestSubStr",exception);
			throw new ServiceException(ErrorCatalog.APPLICATION_ERROR,exception);
		}
		return longestsubstrResponseDTO ;
	}

}
