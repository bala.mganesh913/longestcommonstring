package com.test.longestsubstr.exception;

import lombok.Getter;

@Getter
public class InvalidInputException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ErrorCatalog errorCatalog;

    public InvalidInputException(ErrorCatalog errorCatalog)
    {
        super(errorCatalog.getMessage());
        this.errorCatalog = errorCatalog;
    }

    public InvalidInputException(ErrorCatalog errorCatalog, String message)
    {
        super(message);
        this.errorCatalog = errorCatalog;
    }

    public InvalidInputException(ErrorCatalog errorCatalog, Throwable throwable)
    {
        super(errorCatalog.getMessage(),throwable);
        this.errorCatalog = errorCatalog;
    }
}
