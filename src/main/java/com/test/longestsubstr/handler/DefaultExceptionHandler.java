package com.test.longestsubstr.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.test.longestsubstr.dto.ErrorDto;
import com.test.longestsubstr.exception.ErrorCatalog;

@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> handleException (Exception exception)
    {
        int code = ErrorCatalog.APPLICATION_ERROR.getCode();
        String message = exception.getMessage();
        ErrorDto errorDetails = new ErrorDto(code,message);
        log.error("Error while calling api",exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
    }


}
